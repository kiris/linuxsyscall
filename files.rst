================
 ファイル置き場
================

:download:`Linux Kernel 3.4.4 </_static/linux-3.4.4.tar.bz2>`
                 `ここらへん <http://www.kernel.org/>`_ からとってきたもの。


:download:`glibc 2.16 </_static/glibc-2.16.zip>`
                 `ここらへん <http://www.gnu.org/software/libc/>`_ からとってきたもの。
